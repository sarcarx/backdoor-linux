#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <stdio.h>
#include <string.h>

int main() {
  printf("pid: %d\n", (int) getpid());
  while(1) {
    sleep(1);
  }
  return 0;
}
