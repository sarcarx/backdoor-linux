#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>

#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <fcntl.h>

#define SERV_PORT 34566
#define SERV_IP "127.0.0.1"
#define BUFFER_SIZE 1024

void error(const char* msg) {
  fprintf(stderr, "%s\n", msg);
  exit(EXIT_FAILURE);
}

int myWrite(int fd, const void* buffer, size_t count) {
  ssize_t written = 0;
  while(written < (ssize_t) count) {
    written += write(fd, (const char*) buffer + written, count - written);
    if(written == -1) {
      return -1;
    }
  }
  return written;
}

int main(int argc, char** argv) {
  (void) argv;
  (void) argc;
  
  int sockfd, newsockfd, n;
  socklen_t clilen;
  char* buffer;
  struct sockaddr_in serv_addr, cli_addr;
  
  buffer = malloc(BUFFER_SIZE);
  
  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if(sockfd < 0) {
    error("ERROR Opening socket");
  }
  
  bzero((char*) &serv_addr, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  serv_addr.sin_port = htons(SERV_PORT);
  
  if(bind(sockfd, (struct sockaddr*) &serv_addr), sizeof(serv_addr) < 0) {
    error("ERROR Binding");
  }

  listen(sockfd, 10);
  
}
