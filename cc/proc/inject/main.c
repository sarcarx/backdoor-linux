#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

int main() {
  printf("Hello. I've been injected!\n");
  if(getuid() == 0)
    printf("I'm running as root!\n");
  else
    printf("I'm not running as root :'(\n");
  return EXIT_SUCCESS;
}
